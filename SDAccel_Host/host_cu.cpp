#include "xcl2.hpp"
#include <stdlib.h>
#include <vector>
#include <algorithm>

#define M_DATA_TYPE float

uint64_t get_duration_ns(const cl::Event &);

class FPGA_Data {
    public:
        FPGA_Data(cl::Context &c, cl::CommandQueue &q, cl::CommandQueue &q2, std::vector<cl::Device> &devices);
        int initialProgramAndKernel(std::string &, const char *, const char *);
        uint64_t executeFPGA(std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             int,
                             int);
    private:
        std::vector<cl::Device> devices;
        cl::Context context;
        cl::CommandQueue queue;
        cl::CommandQueue queue2;
        cl::Program program;
        cl::Kernel kernel1;
        cl::Kernel kernel2;
};

FPGA_Data::FPGA_Data(cl::Context &c, cl::CommandQueue &q, cl::CommandQueue &q2, std::vector<cl::Device> &ds) {
    context = c;
    queue = q;
    queue2 = q2;
    devices = ds;
}

int FPGA_Data::initialProgramAndKernel(std::string &binaryFile, const char *kn, const char *kn2) {
    cl_int err;
    unsigned fileBufSize;
    auto fileBuf = xcl::read_binary_file(binaryFile, fileBufSize);

    cl::Program::Binaries bins{{fileBuf, fileBufSize}};

    OCL_CHECK(err, program = cl::Program(context, devices, bins, NULL, &err));

    OCL_CHECK(err, kernel1 = cl::Kernel(program, kn, &err));

    OCL_CHECK(err, kernel2 = cl::Kernel(program, kn2, &err));

    delete[] fileBuf;
    return err;
}

void event_cb(cl_event e, cl_int cmdStatus, void *data) {
    cl_command_type comm;
    clGetEventInfo(e, CL_EVENT_COMMAND_TYPE, sizeof(cl_command_type), &comm, NULL);

    cl_int status;
    clGetEventInfo(e, CL_EVENT_COMMAND_EXECUTION_STATUS, sizeof(cl_int), &status, NULL);

    switch(status) {
    case CL_QUEUED:
        std::cout << "Queued :";
        break;
    case CL_SUBMITTED:
        std::cout << "Submitted :";
        break;
    case CL_RUNNING:
        std::cout << "Executing :";
        break;
    case CL_COMPLETE:
        std::cout << "Completed :";
        break;
    }

    std::cout << (char*)(data) << std::endl;
}

uint64_t FPGA_Data::executeFPGA(std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_in1,
                           std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_in2,
                           std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_out1,
                           std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_out2,
                           std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_out3,
                           std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_out4,
                           int dim1,
                           int dim2) {
    cl_int err;
    cl::Event event1;
    cl::Event event2;
    uint64_t kernel_duration = 0;

    OCL_CHECK(err,
              cl::Buffer buffer_in1(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE,
                                    dim1*dim1*sizeof(M_DATA_TYPE),
                                    source_in1.data(),
                                    &err));
    OCL_CHECK(err,
              cl::Buffer buffer_output1(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,
                                    dim1*sizeof(M_DATA_TYPE),
                                    source_out1.data(),
                                    &err));
    
    OCL_CHECK(err,
              cl::Buffer buffer_output2(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE,
                                    dim1*dim1*sizeof(M_DATA_TYPE),
                                    source_out2.data(),
                                    &err));
    OCL_CHECK(err,
              cl::Buffer buffer_in2(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE,
                                    dim2*dim2*sizeof(M_DATA_TYPE),
                                    source_in2.data(),
                                    &err));
    OCL_CHECK(err,
              cl::Buffer buffer_output3(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,
                                    dim2*sizeof(M_DATA_TYPE),
                                    source_out3.data(),
                                    &err));
    
    OCL_CHECK(err,
              cl::Buffer buffer_output4(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE,
                                    dim2*dim2*sizeof(M_DATA_TYPE),
                                    source_out4.data(),
                                    &err));

        OCL_CHECK(err, err = kernel1.setArg(0, buffer_in1));
        OCL_CHECK(err, err = kernel1.setArg(1, buffer_output1));
        OCL_CHECK(err, err = kernel1.setArg(2, buffer_output2));
        OCL_CHECK(err, err = kernel1.setArg(3, dim1));
        OCL_CHECK(err, err = kernel2.setArg(0, buffer_in2));
        OCL_CHECK(err, err = kernel2.setArg(1, buffer_output3));
        OCL_CHECK(err, err = kernel2.setArg(2, buffer_output4));
        OCL_CHECK(err, err = kernel2.setArg(3, dim2));

        OCL_CHECK(err,
                  err = queue.enqueueMigrateMemObjects({buffer_in1}, 0));
        OCL_CHECK(err,
                  err = queue2.enqueueMigrateMemObjects({buffer_in2}, 0));

        OCL_CHECK(err, err = queue.enqueueTask(kernel1, NULL, &event1));
        event1.setCallback(CL_COMPLETE, event_cb, (void *)"kernel1");

        OCL_CHECK(err, err = queue2.enqueueTask(kernel2, NULL, &event2));
        event2.setCallback(CL_COMPLETE, event_cb, (void *)"kernel2");

        OCL_CHECK(err,
                  err = queue.enqueueMigrateMemObjects({buffer_in1, buffer_output1, buffer_output2},
                                                       CL_MIGRATE_MEM_OBJECT_HOST));
        OCL_CHECK(err,
                  err = queue2.enqueueMigrateMemObjects({buffer_in2, buffer_output3, buffer_output4},
                                                       CL_MIGRATE_MEM_OBJECT_HOST));

    OCL_CHECK(err, err = queue.finish());
    OCL_CHECK(err, err = queue2.finish());

    kernel_duration = get_duration_ns(event1);
    std::cout << kernel_duration << std::endl;
    kernel_duration = get_duration_ns(event2);
    std::cout << kernel_duration << std::endl;

    return kernel_duration;
}

uint64_t get_duration_ns(const cl::Event &event) {
    cl_int err;
    uint64_t nstimestart, nstimeend;
    OCL_CHECK(err,
              err = event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_START,
                                                     &nstimestart));
    OCL_CHECK(err,
              err = event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_END,
                                                     &nstimeend));
    return (nstimeend - nstimestart);
}

uint64_t run_fpga(
                 FPGA_Data &fdata,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_in1,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_in2,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_out1,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_out2,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_out3,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_out4,
                 int dim1,
                 int dim2) {

    uint64_t kernel_duration = 0;

    kernel_duration = fdata.executeFPGA(source_in1, source_in2, source_out1, source_out2, source_out3, source_out4, dim1, dim2);

    return kernel_duration;
}

int main(int argc, char **argv) {
    if (argc != 7) {
        std::cout << "Usage: " << argv[0] << " <XCLBIN File> " << "<Kernel Name1> " << "<Kernel Name 2> "<< "<dimension size1> <dimension size2> " << "<if print matrix> "<< std::endl;
        return EXIT_FAILURE;
    }
    cl_int err;
    uint64_t kernel_duration = 0;
    int dim1 = atoi(argv[4]);
    int dim2 = atoi(argv[5]);
    int ifprint = atoi(argv[6]);
    std::string FileName = argv[1];

    if (dim1 < 2 || dim2 < 2) {
        std::cout << "Size of dimension cannot smaller than 2" << std::endl;
    }

    M_DATA_TYPE data[16] = {4,-30,60,-35, -30, 300, -675, 420, 60, -675, 1620, -1050, -35, 420, -1050, 700};

    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_in1(dim1*dim1*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_in2(dim2*dim2*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out1(dim1*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out2(dim1*dim1*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out3(dim2*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out4(dim2*dim2*sizeof(M_DATA_TYPE));

    std::vector<cl::Device> devices = xcl::get_xil_devices();
    auto device = devices[0];

    OCL_CHECK(err, cl::Context context(device, NULL, NULL, NULL, &err));

    OCL_CHECK(
        err,
        cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE, &err));
    OCL_CHECK(
        err,
        cl::CommandQueue queue2(context, device, CL_QUEUE_PROFILING_ENABLE, &err));

    auto device_name = device.getInfo<CL_DEVICE_NAME>();

    std::cout << "Device Name: " << device_name << std::endl;

    devices.resize(1);

    FPGA_Data fdata(context, queue, queue2, devices);
    fdata.initialProgramAndKernel(FileName, argv[2], argv[3]);

    for (int i=0; i<dim1; i++) {
        for (int j=0; j<dim1; j++) {
            source_in1[i*dim1+j] = j+1;
            source_in1[j*dim1+i] = j+1;
            //source_in1[i*dim+j] = data[i*dim+j];
            source_out2[i*dim1+j] = 0.0;
        }
        source_out1[i] = 0.0;
    }

    for (int i=0; i<dim2; i++) {
        for (int j=0; j<dim2; j++) {
            source_in2[i*dim2+j] = j+1;
            source_in2[j*dim2+i] = j+1;
            //source_in1[i*dim+j] = data[i*dim+j];
            source_out4[i*dim2+j] = 0.0;
        }
        source_out3[i] = 0.0;
    }
    kernel_duration = run_fpga(
        fdata, source_in1, source_in2, source_out1, source_out2, source_out3, source_out4, dim1, dim2);

    std::cout << std::endl << source_in1[1] << std::endl << source_in2[1] << std::endl;

    if (ifprint) {
        for (int i=0; i<dim1; i++) {
            printf("%f, ", source_out1[i]);
        }
        printf("\n\n");
        for (int i=0; i<dim2; i++) {
            printf("%f, ", source_out3[i]);
        }
        printf("\n\n");

        for (int i=0; i<dim1*dim1; i++) {
            if (i != 0 && i % dim1 == 0)
                printf("\n");
            printf("%f, ", source_out2[i]);
        }
        printf("\n\n");
    }

    return 0;
}
