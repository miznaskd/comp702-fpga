#include "xcl2.hpp"
#include <stdlib.h>
#include <vector>
#include <algorithm>

#define M_DATA_TYPE float

uint64_t get_duration_ns(const cl::Event &event) {
    cl_int err;
    uint64_t nstimestart, nstimeend;
    OCL_CHECK(err,
              err = event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_START,
                                                     &nstimestart));
    OCL_CHECK(err,
              err = event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_END,
                                                     &nstimeend));
    return (nstimeend - nstimestart);
}

uint64_t run_fpga(
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &source_in1,
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
        &source_fpga_results1,
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
        &source_fpga_results2,
    int dim,
    std::string &binaryFile
) {
    cl_int err;
    unsigned fileBufSize;

    auto devices = xcl::get_xil_devices();
    auto device = devices[0];

    OCL_CHECK(err, cl::Context context(device, NULL, NULL, NULL, &err));
    OCL_CHECK(
        err,
        cl::CommandQueue q(context, device, CL_QUEUE_PROFILING_ENABLE, &err));
    auto device_name = device.getInfo<CL_DEVICE_NAME>();

    auto fileBuf = xcl::read_binary_file(binaryFile, fileBufSize);
    cl::Program::Binaries bins{{fileBuf, fileBufSize}};
    devices.resize(1);
    OCL_CHECK(err, cl::Program program(context, devices, bins, NULL, &err));

    OCL_CHECK(err, cl::Kernel kernel(program, "QR_Symm", &err));

    OCL_CHECK(err,
              cl::Buffer buffer_in1(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY,
                                    dim*dim*sizeof(M_DATA_TYPE),
                                    source_in1.data(),
                                    &err));
    OCL_CHECK(err,
              cl::Buffer buffer_output1(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,
                                    dim*sizeof(M_DATA_TYPE),
                                    source_fpga_results1.data(),
                                    &err));
    
    OCL_CHECK(err,
              cl::Buffer buffer_output2(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,
                                    dim*dim*sizeof(M_DATA_TYPE),
                                    source_fpga_results2.data(),
                                    &err));

    //Set the kernel arguments
    int narg = 0;
    OCL_CHECK(err, err = kernel.setArg(narg++, buffer_in1));
    OCL_CHECK(err, err = kernel.setArg(narg++, buffer_output1));
    OCL_CHECK(err, err = kernel.setArg(narg++, buffer_output2));
    OCL_CHECK(err, err = kernel.setArg(narg++, dim));

    OCL_CHECK(err,
              err = q.enqueueMigrateMemObjects({buffer_in1},
                                               0 /* 0 means from host*/));

    cl::Event event;
    uint64_t kernel_duration = 0;

    OCL_CHECK(err, err = q.enqueueTask(kernel, NULL, &event));

    OCL_CHECK(err,
              err = q.enqueueMigrateMemObjects({buffer_output1, buffer_output2},
                                               CL_MIGRATE_MEM_OBJECT_HOST));
    OCL_CHECK(err, err = q.finish());
    delete[] fileBuf;

    kernel_duration = get_duration_ns(event);

    return kernel_duration;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        std::cout << "Usage: " << argv[0] << " <XCLBIN File>" << std::endl;
        return EXIT_FAILURE;
    }
    int dim = 4;

    M_DATA_TYPE data[16] = {4,-30,60,-35, -30, 300, -675, 420, 60, -675, 1620, -1050, -35, 420, -1050, 700};

    std::string binaryFile = argv[1];

    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_in1(dim*dim*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out1(dim*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out2(dim*dim*sizeof(M_DATA_TYPE));

    for (int i = 0; i < dim*dim; i++) {
        source_in1[i] = data[i];
        source_out1[i] = 0.0;
        source_out2[i] = 0.0;
    }

    uint64_t kernel_duration = 0;

    kernel_duration = run_fpga(
        source_in1, source_out1, source_out2, dim, binaryFile);

    for (int i=0; i<dim; i++) {
        printf("%f, ", source_out1[i]);
    }
    printf("\n\n");

    for (int i=0; i<dim*dim; i++) {
        if (i != 0 && i % dim == 0)
            printf("\n");
        printf("%f, ", source_out2[i]);
    }
    printf("\n\n");

    std::cout << kernel_duration << std::endl;

    return 0;
}
