#include "xcl2.hpp"
#include <stdlib.h>
#include <vector>
#include <algorithm>

#define M_DATA_TYPE float

uint64_t get_duration_ns(const cl::Event &);

class FPGA_Data {
    public:
        FPGA_Data(cl::Context &c, cl::CommandQueue &q, std::vector<cl::Device> &devices);
        int initialProgramAndKernel(std::string &, const char *);
        uint64_t executeFPGA(std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> &,
                             int);
    private:
        std::vector<cl::Device> devices;
        cl::Context context;
        cl::CommandQueue queue;
        cl::Program program;
        cl::Kernel kernel;
};

FPGA_Data::FPGA_Data(cl::Context &c, cl::CommandQueue &q, std::vector<cl::Device> &ds) {
    context = c;
    queue = q;
    devices = ds;
}

int FPGA_Data::initialProgramAndKernel(std::string &binaryFile, const char *kn) {
    cl_int err;
    unsigned fileBufSize;
    auto fileBuf = xcl::read_binary_file(binaryFile, fileBufSize);

    cl::Program::Binaries bins{{fileBuf, fileBufSize}};

    OCL_CHECK(err, program = cl::Program(context, devices, bins, NULL, &err));

    OCL_CHECK(err, kernel = cl::Kernel(program, kn, &err));

    delete[] fileBuf;
    return err;
}

uint64_t FPGA_Data::executeFPGA(std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_in1,
                           std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_out1,
                           std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                                &source_out2,
                           int dim) {
    cl_int err;
    cl::Event event;
    uint64_t kernel_duration = 0;

    OCL_CHECK(err,
              cl::Buffer buffer_in1(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE,
                                    dim*dim*sizeof(M_DATA_TYPE),
                                    source_in1.data(),
                                    &err));
    OCL_CHECK(err,
              cl::Buffer buffer_output1(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY,
                                    dim*sizeof(M_DATA_TYPE),
                                    source_out1.data(),
                                    &err));
    
    OCL_CHECK(err,
              cl::Buffer buffer_output2(context,
                                    CL_MEM_USE_HOST_PTR | CL_MEM_READ_WRITE,
                                    dim*dim*sizeof(M_DATA_TYPE),
                                    source_out2.data(),
                                    &err));

    int narg = 0;
    OCL_CHECK(err, err = kernel.setArg(narg++, buffer_in1));
    OCL_CHECK(err, err = kernel.setArg(narg++, buffer_output1));
    OCL_CHECK(err, err = kernel.setArg(narg++, buffer_output2));
    OCL_CHECK(err, err = kernel.setArg(narg++, dim));

    OCL_CHECK(err,
              err = queue.enqueueMigrateMemObjects({buffer_in1}, 0));

    OCL_CHECK(err, err = queue.enqueueTask(kernel, NULL, &event));

    OCL_CHECK(err,
              err = queue.enqueueMigrateMemObjects({buffer_in1, buffer_output1, buffer_output2},
                                               CL_MIGRATE_MEM_OBJECT_HOST));
    OCL_CHECK(err, err = queue.finish());

    kernel_duration = get_duration_ns(event);

    return kernel_duration;
}

uint64_t get_duration_ns(const cl::Event &event) {
    cl_int err;
    uint64_t nstimestart, nstimeend;
    OCL_CHECK(err,
              err = event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_START,
                                                     &nstimestart));
    OCL_CHECK(err,
              err = event.getProfilingInfo<uint64_t>(CL_PROFILING_COMMAND_END,
                                                     &nstimeend));
    return (nstimeend - nstimestart);
}

uint64_t run_fpga(
                 FPGA_Data &fdata,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
	                  &source_in1,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_out1,
                 std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>>
                      &source_out2,
                 int dim) {

    uint64_t kernel_duration = 0;

    kernel_duration = fdata.executeFPGA(source_in1, source_out1, source_out2, dim);

    return kernel_duration;
}

int main(int argc, char **argv) {
    if (argc != 5) {
        std::cout << "Usage: " << argv[0] << " <XCLBIN File>" << "<Kernel Name>" << "<dimension size>" << "<if print matrix>"<< std::endl;
        return EXIT_FAILURE;
    }
    cl_int err;
    uint64_t kernel_duration = 0;
    int dim = atoi(argv[3]);
    int ifprint = atoi(argv[4]);
    std::string FileName = argv[1];

    if (dim < 2) {
        std::cout << "Size of dimension cannot smaller than 2" << std::endl;
    }

    M_DATA_TYPE data[16] = {4,-30,60,-35, -30, 300, -675, 420, 60, -675, 1620, -1050, -35, 420, -1050, 700};

    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_in1(dim*dim*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out1(dim*sizeof(M_DATA_TYPE));
    std::vector<M_DATA_TYPE, aligned_allocator<M_DATA_TYPE>> source_out2(dim*dim*sizeof(M_DATA_TYPE));

    std::vector<cl::Device> devices = xcl::get_xil_devices();
    auto device = devices[0];

    OCL_CHECK(err, cl::Context context(device, NULL, NULL, NULL, &err));

    OCL_CHECK(
        err,
        cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE, &err));

    auto device_name = device.getInfo<CL_DEVICE_NAME>();

    std::cout << "Device Name: " << device_name << std::endl;

    devices.resize(1);

    FPGA_Data fdata(context, queue, devices);
    fdata.initialProgramAndKernel(FileName, argv[2]);

    for (int i=0; i<dim; i++) {
        for (int j=0; j<dim; j++) {
            source_in1[i*dim+j] = j+1;
            source_in1[j*dim+i] = j+1;
            //source_in1[i*dim+j] = data[i*dim+j];
            source_out2[i*dim+j] = 0.0;
        }
        source_out1[i] = 0.0;
    }

    kernel_duration = run_fpga(
        fdata, source_in1, source_out1, source_out2, dim);

    std::cout << kernel_duration << std::endl << source_in1[1] << std::endl;

    if (ifprint) {
        for (int i=0; i<dim; i++) {
            printf("%f, ", source_out1[i]);
        }
        printf("\n\n");

        for (int i=0; i<dim*dim; i++) {
            if (i != 0 && i % dim == 0)
                printf("\n");
            printf("%f, ", source_out2[i]);
        }
        printf("\n\n");
    }

    return 0;
}
