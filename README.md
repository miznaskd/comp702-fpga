# COMP702 FPGA

* How to setup pynq z2
	1. Download the image from website
	2. Setup the source jumper to SD
	3. Setup the power source jumper to USB
	4. Insert SD card into the board
	5. Connect RJ45 port
	6. Connect Micro USB
	7. Turn on

* How to upload image
	* For Mac/Linux
		* Insert SD card to computer
		* Unmount the mounted device
			* For MAC: diskutil umount (mount point)
			* For Linux: sudo umount (mount point)
		* sudo dd bs=1m if=image-file of=/dev/(disk name)

* Network Configuration
	* DHCP
	* Static
		* 192.168.2.99 (Default IP)

* How to connect to the board
	* Connect to jupyter
		* http://ip-address
	* SSH
		* ssh xilinx@(ip-address)
	* Password
		* xilinx (Default)
