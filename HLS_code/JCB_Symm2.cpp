extern "C" {

#include <stdint.h>
#include <math.h>

#ifdef SDACCEL
#define MAX_SIZE 500
#else
#define MAX_SIZE 100
#endif

#define M_DATA_TYPE float

M_DATA_TYPE JCB_abs(M_DATA_TYPE val) {
    if (val < 0.0)
        return -val;
    return val;
}

void JCB_Calc_Givens(M_DATA_TYPE e1,
                     M_DATA_TYPE e2,
                     M_DATA_TYPE e3,
                     M_DATA_TYPE *gm,
                     int32_t dim,
                     uint32_t p,
                     uint32_t q) {
#pragma HLS inline off
    int32_t i, j;
    M_DATA_TYPE sin, cos, tan, w, tw1, tw2, sw, st;

    tw1 = (e3-e1);
    tw2 = (2*e2);
    w = tw1 / tw2;
    sw = sqrt((w*w)+1);
    if (w < 0)
        tan = -w - sw;
    else
        tan = -w + sw;
    st = sqrt((tan*tan)+1);
    sin = tan / st;
    cos = 1 / st;

    gm[0] = sin;
    gm[1] = cos;
}

void JCB_doGAG(M_DATA_TYPE *m, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y, M_DATA_TYPE *newm) {
    M_DATA_TYPE temp1[MAX_SIZE];
    M_DATA_TYPE temp2[MAX_SIZE];
    M_DATA_TYPE temp3[MAX_SIZE];
    M_DATA_TYPE temp4[MAX_SIZE];
    M_DATA_TYPE temp5[MAX_SIZE];
    M_DATA_TYPE temp6[MAX_SIZE];
    static M_DATA_TYPE csaj, ccai, ssai, ssbj, ccbj;
    int32_t i;

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        temp1[i] = m[x*dim+i] * cos;
        temp2[i] = m[x*dim+i] * sin;
    }
    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        temp3[i] = m[y*dim+i] * sin;
        temp4[i] = m[y*dim+i] * cos;
    }

    ccai = temp1[x] * cos;
    ssai = temp2[x] * sin;
    csaj = temp1[y] * sin * 2;
    ssbj = temp3[y] * sin;
    ccbj = temp4[y] * cos;

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        m[x*dim+i] = temp1[i] - temp3[i];
        temp5[i] = temp1[i] - temp3[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        m[i*dim+x] = temp5[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        m[y*dim+i] = temp2[i] + temp4[i];
        temp6[i] = temp2[i] + temp4[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        m[i*dim+y] = temp6[i];
    }

    ccai -= csaj;
    ssai += csaj;
    ccai += ssbj;
    ssai += ccbj;
    newm[x] = ccai;
    m[x*dim+x] = ccai;
    m[x*dim+y] = 0.0;
    newm[y] = ssai;
    m[y*dim+x] = 0.0;
    m[y*dim+y] = ssai;
}

void JCB_doAG(M_DATA_TYPE *evs, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y) {
    int32_t i;
    M_DATA_TYPE temp1[MAX_SIZE];
    M_DATA_TYPE temp2[MAX_SIZE];
    M_DATA_TYPE temp3[MAX_SIZE];
    M_DATA_TYPE temp4[MAX_SIZE];

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        temp1[i] = evs[i*dim+x] * cos;
        temp2[i] = evs[i*dim+x] * sin;
    }
    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        temp3[i] = evs[i*dim+y] * sin;
        temp4[i] = evs[i*dim+y] * cos;
    }
    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        evs[i*dim+x] = temp1[i] - temp3[i];
    }
    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
    #pragma HLS unroll factor=2
        evs[i*dim+y] = temp2[i] + temp4[i];
    }
}

void JCB_doCalc(M_DATA_TYPE *m, M_DATA_TYPE *evs, M_DATA_TYPE *gm, int32_t dim, int32_t i, int32_t j, M_DATA_TYPE *newm) {
#pragma HLS inline off
    JCB_doGAG(m, dim, gm[0], gm[1], i, j, newm);
    JCB_doAG(evs, dim, gm[0], gm[1], i, j);
}

int32_t JCB_convergence(M_DATA_TYPE *m1, M_DATA_TYPE *old, int32_t dim, int32_t row1, int32_t row2) {
    int32_t ret = 1;
    int32_t i = 0;

    if (m1[row1] != old[row1]) {
        old[row1] = m1[row1];
        ret = 0;
    }

    if (m1[row2] != old[row2]) {
        old[row2] = m1[row2];
        ret = 0;
    }

    return ret;
}

void update_pivot_in_row(M_DATA_TYPE *mtemp, int32_t row, int32_t *ppos, int32_t dim) {
    int32_t row_pos = row;
    int32_t row_last = dim;
    int32_t pos = row_pos + 1;
    int32_t end = dim-1;
    int32_t totalele = end - pos + 1;
    int32_t temp[MAX_SIZE];
    int32_t temp2[MAX_SIZE];
    int32_t i;
    for (i=0; i<totalele; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=1 max=499
    #pragma HLS pipeline
    #pragma HLS unroll factor=2
        temp[i] = pos + i;
        temp2[i] = pos + i;
    }

    while (totalele != 1) {
        if (totalele % 2 == 1) {
            if (JCB_abs(mtemp[temp[totalele-2]]) < JCB_abs(mtemp[temp[totalele-1]])) {
                temp[totalele-2] = temp[totalele-1];
                temp2[totalele-2] = temp[totalele-1];
            }
            totalele -= 1;
        }
        totalele /= 2;
        for (i=0; i<totalele; i++) {
        #pragma HLS LOOP_TRIPCOUNT min=1 max=500
        #pragma HLS pipeline
        #pragma HLS dependence array inter false
            if (JCB_abs(mtemp[temp[i]]) < JCB_abs(mtemp[temp[i+totalele]])) {
                temp2[i] = temp[i+totalele];
            }
        }
        for (i=0; i<totalele; i++) {
        #pragma HLS LOOP_TRIPCOUNT min=1 max=500
        #pragma HLS pipeline
        #pragma HLS unroll factor=2
            temp[i] = temp2[i];
        }
    }

    *ppos = temp[0];
}

void JCB_checkRemainPivotValue(M_DATA_TYPE *row1temp, M_DATA_TYPE *row2temp, M_DATA_TYPE *pvtval, int32_t *pvtpos, int32_t row1, int32_t row2, int32_t dim) {
    int32_t i;
    for (i=0; i<row2+1; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=1 max=498
    #pragma HLS pipeline
        int32_t trow2 = pvtpos[i];
        if (i==row1 || i==row2)
            continue;
        if (trow2 == row1) {
            pvtval[i] = row1temp[i];
        } else if (trow2 == row2){
            pvtval[i] = row2temp[i];
        }
    }
}

void JCB_update_pivot_single_1(M_DATA_TYPE *m1, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row, int32_t dim) {
    uint32_t i;
    int32_t pos;

    update_pivot_in_row(&m1[row*dim], row, &pos, dim);
    pvtpos[row] = pos;
    pvtval[row] = m1[row*dim+pos];
}

void JCB_update_pivot_double(M_DATA_TYPE *m1, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row1, int32_t row2, int32_t dim) {
    int32_t pos1, pos2;
    uint32_t i;
    M_DATA_TYPE row1temp[MAX_SIZE];
    M_DATA_TYPE row2temp[MAX_SIZE];
    M_DATA_TYPE row1tempf[MAX_SIZE];
    M_DATA_TYPE row2tempf[MAX_SIZE];

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
        if (i<row1)
            row1tempf[i] = m1[row1*dim+i];
        if (i>row1)
            row1temp[i] = m1[row1*dim+i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
        if (i<row2)
            row2tempf[i] = m1[row2*dim+i];
        if (i>row2)
            row2temp[i] = m1[row2*dim+i];
    }

    update_pivot_in_row(row1temp, row1, &pos1, dim);
    update_pivot_in_row(row2temp, row2, &pos2, dim);
    JCB_checkRemainPivotValue(row1tempf, row2tempf, pvtval, pvtpos, row1, row2, dim);
    pvtpos[row1] = pos1;
    pvtval[row1] = m1[row1*dim+pos1];
    pvtpos[row2] = pos2;
    pvtval[row2] = m1[row2*dim+pos2];
}

void JCB_update_pivot_single_2(M_DATA_TYPE *m1, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row1, int32_t row2, int32_t dim) {
    uint32_t i;
    int32_t pos;
    M_DATA_TYPE row1temp[MAX_SIZE];
    M_DATA_TYPE row2temp[MAX_SIZE];
    M_DATA_TYPE row1tempf[MAX_SIZE];
    M_DATA_TYPE row2tempf[MAX_SIZE];

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
        if (i<row1)
            row1tempf[i] = m1[row1*dim+i];
        if (i>row1)
            row1temp[i] = m1[row1*dim+i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
        if (i<row2)
            row2tempf[i] = m1[row2*dim+i];
        if (i>row2)
            row2temp[i] = m1[row2*dim+i];
    }

    update_pivot_in_row(row1temp, row1, &pos, dim);
    JCB_checkRemainPivotValue(row1tempf, row2tempf, pvtval, pvtpos, row1, row2, dim);
    pvtpos[row1] = pos;
    pvtval[row1] = m1[row1*dim+pos];
}

int32_t JCB_findLargest(M_DATA_TYPE *pvtval, int32_t dim) {
    int32_t itemp[MAX_SIZE];
    int32_t itemp2[MAX_SIZE];
    int32_t i, end;

    end = dim - 1;
    for (i=0; i<end; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
    #pragma HLS unroll factor=2
        itemp[i] = i;
        itemp2[i] = i;
    }

    while (end != 1) {
        if (end % 2 == 1) {
            if (JCB_abs(pvtval[itemp[end-2]]) < JCB_abs(pvtval[itemp[end-1]])) {
                itemp[end-2] = itemp[end-1];
                itemp2[end-2] = itemp2[end-1];
            }
            end -= 1;
        }
        end = end / 2;
        for (i=0; i<end; i++) {
        #pragma HLS LOOP_TRIPCOUNT min=2 max=500
        #pragma HLS pipeline
            if (JCB_abs(pvtval[itemp[i]]) < JCB_abs(pvtval[itemp[i+end]]))
                itemp2[i] = itemp[i+end];
        }
        for (i=0; i<end; i++) {
        #pragma HLS LOOP_TRIPCOUNT min=2 max=500
        #pragma HLS pipeline
        #pragma HLS unroll factor=2
            itemp[i] = itemp2[i];
        }
    }
    return itemp[0];
}

void JCB_doFinal(M_DATA_TYPE *evectors,
                 M_DATA_TYPE *out_matrix1,
                 M_DATA_TYPE *out_matrix2,
                 M_DATA_TYPE *old,
                 int32_t dim) {
#pragma HLS inline off
#pragma HLS dataflow
    int32_t i;

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        out_matrix1[i] = old[i];
    }

    for (i=0; i<dim*dim; i++) {
    #pragma HLS pipeline
        out_matrix2[i] = evectors[i];
    }
}

void JCB_Symm2(M_DATA_TYPE *in_matrix,
              M_DATA_TYPE *out_matrix1,
              M_DATA_TYPE *out_matrix2,
              int32_t dim) {
#pragma HLS INTERFACE m_axi port = in_matrix offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_matrix1 offset = slave bundle = gmem1
#pragma HLS INTERFACE m_axi port = out_matrix2 offset = slave bundle = gmem2
#pragma HLS INTERFACE s_axilite port = in_matrix bundle = control
#pragma HLS INTERFACE s_axilite port = out_matrix1 bundle = control
#pragma HLS INTERFACE s_axilite port = out_matrix2 bundle = control
#pragma HLS INTERFACE s_axilite port = dim bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control

    int32_t i = 0, iters = 0;
    int32_t convergence = 0;
    int32_t pvtpos[MAX_SIZE];
    M_DATA_TYPE pvtval[MAX_SIZE];
    M_DATA_TYPE old[MAX_SIZE];
    M_DATA_TYPE newm[MAX_SIZE];
    M_DATA_TYPE m[MAX_SIZE*MAX_SIZE];
    M_DATA_TYPE evectors[MAX_SIZE*MAX_SIZE];

    for (i=0; i<dim*dim; i++) {
    #pragma HLS pipeline
        m[i] = in_matrix[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS LOOP_TRIPCOUNT min=2 max=500
    #pragma HLS pipeline
        old[i] = m[i*dim+i];
        newm[i] = old[i];
        evectors[i*dim+i] = 1.0;
    }

    for (i=0; i<dim-1; i++) {
        int32_t j;
        JCB_update_pivot_single_1(m, pvtpos, pvtval, i, dim);
    }

    while (!convergence) {
        M_DATA_TYPE e1, e2, e3;
        int32_t row1, row2;

        row1 = JCB_findLargest(pvtval, dim);
        row2 = pvtpos[row1];
        e1 = m[row1*dim+row1];
        e3 = m[row2*dim+row2];
        e2 = m[row2*dim+row1];

        if (e2 != 0) {
            static M_DATA_TYPE gm[2];
            JCB_Calc_Givens(e1, e2, e3, gm, dim, row1, row2);
            JCB_doCalc(m, evectors, gm, dim, row1, row2, newm);

            if (row2 != dim-1) {
                JCB_update_pivot_double(m, pvtpos, pvtval, row1, row2, dim);
            } else {
                JCB_update_pivot_single_2(m, pvtpos, pvtval, row1, row2, dim);
            }
        }
        convergence = JCB_convergence(newm, old, dim, row1, row2);
        iters ++;
    }
    M_DATA_TYPE fiters = (M_DATA_TYPE)iters;
    JCB_doFinal(evectors, out_matrix1, out_matrix2, old, dim);
    in_matrix[1] = fiters;
}

}
