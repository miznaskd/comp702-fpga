extern "C" {

#include <math.h>
#include <stdint.h>

#ifdef SDACCEL
#define MAX_SIZE 10000
#else
#define MAX_SIZE 100
#endif

#define M_DATA_TYPE float

M_DATA_TYPE QR_copy_sign(M_DATA_TYPE num, M_DATA_TYPE val) {
    if (val < 0)
        return -num;
    return num;
}

M_DATA_TYPE QR_abs(M_DATA_TYPE val) {
    if (val < 0.0)
        return -val;
    return val;
}

void QR_Calc_Givens(M_DATA_TYPE *gm,
                    M_DATA_TYPE e1,
                    M_DATA_TYPE e2,
                    int32_t dim) {
    int32_t i, j;
    M_DATA_TYPE sin, cos, tan, radius;

    if (e2 == 0.0) {
        cos = QR_copy_sign(1, e1);
        sin = 0;
        //radius = QR_abs(e1);
    } else if (e1 == 0.0) {
        cos = 0;
        sin = -QR_copy_sign(1, e2);
        //radius = QR_abs(e2);
    } else if (QR_abs(e2) > QR_abs(e1)) {
        M_DATA_TYPE u = 0.0, t;
        tan = e1/e2;
        t = sqrt(1+tan*tan);
        u = QR_copy_sign(t, e2);
        sin = -1/u;
        cos = -sin*tan;
        //radius = u*e2;
    } else {
        M_DATA_TYPE u = 0.0, t;
        tan = e2/e1;
        t = sqrt(1+tan*tan);
        u = QR_copy_sign(t, e1);
        cos = 1/u;
        sin = -cos*tan;
        //radius = u*e1;
    }
    gm[0] = sin;
    gm[1] = cos;
}

void QR_doGAG(M_DATA_TYPE *m, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y) {
    M_DATA_TYPE temp1[MAX_SIZE];
    M_DATA_TYPE temp2[MAX_SIZE];
    M_DATA_TYPE temp3[MAX_SIZE];
    M_DATA_TYPE temp4[MAX_SIZE];
    M_DATA_TYPE a[MAX_SIZE];
    M_DATA_TYPE b[MAX_SIZE];
    int32_t i;

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        a[i] = m[x*dim+i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        b[i] = m[y*dim+i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        temp1[i] = a[i] * cos;
        temp2[i] = a[i] * sin;
        temp3[i] = b[i] * sin;
        temp4[i] = b[i] * cos;
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
        temp1[i] -= temp3[i];
        temp2[i] += temp4[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        m[x*dim+i] = temp1[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        m[y*dim+i] = temp2[i];
    }

    a[x] = temp1[x];
    a[y] = temp2[x];
    b[x] = temp1[y];
    b[y] = temp2[y];

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        temp1[i] = a[i] * cos;
        temp2[i] = a[i] * sin;
        temp3[i] = b[i] * sin;
        temp4[i] = b[i] * cos;
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
        temp1[i] -= temp3[i];
        temp2[i] += temp4[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        m[i*dim+x] = temp1[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        m[i*dim+y] = temp2[i];
    }
}

void QR_doAG(M_DATA_TYPE *m, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y) {
    M_DATA_TYPE temp1[MAX_SIZE];
    M_DATA_TYPE temp2[MAX_SIZE];
    M_DATA_TYPE temp3[MAX_SIZE];
    M_DATA_TYPE temp4[MAX_SIZE];
    M_DATA_TYPE a[MAX_SIZE];
    M_DATA_TYPE b[MAX_SIZE];
    int32_t i;

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        a[i] = m[i*dim+x];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        b[i] = m[i*dim+y];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        temp1[i] = a[i] * cos;
        temp2[i] = a[i] * sin;
        temp3[i] = b[i] * sin;
        temp4[i] = b[i] * cos;
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
    #pragma HLS dependence array inter false
        temp1[i] -= temp3[i];
        temp2[i] += temp4[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        m[i*dim+x] = temp1[i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        m[i*dim+y] = temp2[i];
    }
}

void QR_checkdiagonal(M_DATA_TYPE *m, int32_t dim, M_DATA_TYPE *shift_val) {
    int32_t ret = 1, i;
    M_DATA_TYPE temp[MAX_SIZE];

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        temp[i] = m[i*dim+i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        if (*shift_val != temp[i])
            ret = 0;
        temp[i] -= *shift_val;
    }

    if (ret) {
        *shift_val = *shift_val - 1;
        for (i=0; i<dim; i++) {
        #pragma HLS pipeline
            temp[i] += 1;
        }
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
         m[i*dim+i] = temp[i];
    }
}

int32_t QR_convergence(M_DATA_TYPE *m1, M_DATA_TYPE *old, int32_t dim, M_DATA_TYPE shift_val) {
    int32_t ret = 1;
    int32_t i = 0;
    M_DATA_TYPE temp[MAX_SIZE];

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        temp[i] = m1[i*dim+i];
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        temp[i] += shift_val;
        if (old[i] != temp[i]) {
            old[i] = temp[i];
            ret = 0;
        }
    }

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        m1[i*dim+i] = temp[i];
    }

    return ret;
}

void QR_doCalc(M_DATA_TYPE *space1, M_DATA_TYPE *space2, M_DATA_TYPE *gm, int32_t dim, int32_t i, int32_t j) {
#pragma HLS dataflow
    QR_doGAG(space1, dim, gm[0], gm[1], i, j);
    QR_doAG(space2, dim, gm[0], gm[1], i, j);
}

void QR_Symm(M_DATA_TYPE *in_matrix,
             M_DATA_TYPE *out_matrix1,
             M_DATA_TYPE *out_matrix2,
             int32_t dim) {
#pragma HLS INTERFACE m_axi port = in_matrix offset = slave bundle = gmem
#pragma HLS INTERFACE m_axi port = out_matrix1 offset = slave bundle = gmem1
#pragma HLS INTERFACE m_axi port = out_matrix2 offset = slave bundle = gmem1
#pragma HLS INTERFACE s_axilite port = in_matrix bundle = control
#pragma HLS INTERFACE s_axilite port = out_matrix1 bundle = control
#pragma HLS INTERFACE s_axilite port = out_matrix2 bundle = control
#pragma HLS INTERFACE s_axilite port = dim bundle = control
#pragma HLS INTERFACE s_axilite port = return bundle = control

    int32_t i = 0, iters = 0;
    uint32_t start = 0;
    int32_t convergence = 0;
    M_DATA_TYPE *space1 = in_matrix;
    M_DATA_TYPE *space2 = out_matrix2;
    M_DATA_TYPE space4[MAX_SIZE];

    for (i=0; i<dim*dim; i++) {
    #pragma HLS pipeline
        if (i % (dim+1) == 0)
            space2[i] = 1.0;
        else
            space2[i] = 0.0;
    }

    for (i=1; i<dim-1; i++) {
        uint32_t j;
        for (j=i+1; j<dim; j++) {
            M_DATA_TYPE e1 = space1[i*dim+(i-1)];
            M_DATA_TYPE e2 = space1[j*dim+(i-1)];
            if (QR_abs(e2) > 0.0000001) {
                uint32_t idiff = (i-1);
                static M_DATA_TYPE tgm[2];
                QR_Calc_Givens(tgm, e1, e2, dim);
                QR_doCalc(space1, space2, tgm, dim, i, j);
            }
        }
    }

    while (!convergence) {
        M_DATA_TYPE *m = space1;
        M_DATA_TYPE shift_val = m[(dim-1)*dim + (dim-1)];
        QR_checkdiagonal(m, dim, &shift_val);

        for (i=0; i<dim-1; i++) {
            int32_t j = i+1;
            static M_DATA_TYPE gm[2];
            M_DATA_TYPE e1 = m[i*dim+i];
            M_DATA_TYPE e2 = m[j*dim+i];
            if (QR_abs(e2) > 0.0000001) {
                QR_Calc_Givens(gm, e1, e2, dim);
                QR_doCalc(space1, space2, gm, dim, i, j);
            }
        }

        convergence = QR_convergence(m, space4, dim, shift_val);
        iters ++;
    }
    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        out_matrix1[i] = space4[i];
    }
    in_matrix[1] = (M_DATA_TYPE)iters;
}

}
