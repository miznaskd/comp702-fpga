#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>

#ifdef HPC
#define MAX_SIZE 10000
#else
#define MAX_SIZE 100
#endif
#define M_DATA_TYPE float

static uint32_t msize;

void printMatrix(M_DATA_TYPE *m, int32_t dim) {
    uint32_t i;
    for (i=0; i<dim; i++) {
        uint32_t j = 0;
        for (; j<dim; j++) {
            printf("%f, ", m[i*dim+j]);
        }
        printf("\n");
    }
    printf("\n\n");
}

M_DATA_TYPE JCB_abs(M_DATA_TYPE val) {
    if (val < 0.0)
        return -val;
    return val;
}

void JCB_Calc_Givens(M_DATA_TYPE e1,
                     M_DATA_TYPE e2,
                     M_DATA_TYPE e3,
                     M_DATA_TYPE *gm,
                     int32_t dim,
                     uint32_t p,
                     uint32_t q) {
    int32_t i, j;
    M_DATA_TYPE sin, cos, tan, w, tw1, tw2, sw, st;

    tw1 = (e3-e1);
    tw2 = (2*e2);
    w = tw1 / tw2;
    sw = sqrt((w*w)+1);
    if (w < 0)
        tan = -w - sw;
    else
        tan = -w + sw;
    st = sqrt((tan*tan)+1);
    sin = tan / st;
    cos = 1 / st;

    gm[0] = sin;
    gm[1] = cos;
}

void JCB_doGAG(M_DATA_TYPE *xt, M_DATA_TYPE *yt, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y, M_DATA_TYPE *newm) {
    M_DATA_TYPE temp1[MAX_SIZE];
    M_DATA_TYPE temp2[MAX_SIZE];
    M_DATA_TYPE temp3[MAX_SIZE];
    M_DATA_TYPE temp4[MAX_SIZE];
    static M_DATA_TYPE csaj, ccai, ssai, ssbj, ccbj;
    int32_t i;

    for (i=0; i<dim; i++) {
    #pragma HLS pipeline
        temp1[i] = xt[i] * cos;
        temp2[i] = xt[i] * sin;
        temp3[i] = yt[i] * sin;
        temp4[i] = yt[i] * cos;
    }

    ccai = temp1[x] * cos;
    csaj = temp1[y] * sin * 2;
    ssai = temp2[x] * sin;
    ssbj = temp3[y] * sin;
    ccbj = temp4[y] * cos;
    ccai -= csaj;
    ssai += csaj;
    ccai += ssbj;
    ssai += ccbj;

    for (i=0; i<dim; i++) {
		temp1[i] -= temp3[i];
		temp2[i] += temp4[i];
	}

	temp1[x] = ccai;
	temp1[y] = 0.0;
	temp2[x] = 0.0;
	temp2[y] = ssai;

    for (i=0; i<dim; i++) {
        xt[i] = temp1[i];
        yt[i] = temp2[i];
    }
    newm[y] = ssai;
    newm[x] = ccai;
}

void JCB_doAG(M_DATA_TYPE *xt, M_DATA_TYPE *yt, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y) {
    M_DATA_TYPE temp1[MAX_SIZE];
    M_DATA_TYPE temp2[MAX_SIZE];
    M_DATA_TYPE temp3[MAX_SIZE];
    M_DATA_TYPE temp4[MAX_SIZE];
    int32_t i;

    for (i=0; i<dim; i++) {
        temp1[i] = xt[i] * cos - yt[i] * sin;
        temp2[i] = xt[i] * sin + yt[i] * cos;
        xt[i] = temp1[i];
        yt[i] = temp2[i];
    }
}

void JCB_doCalc(M_DATA_TYPE *it, M_DATA_TYPE *jt, M_DATA_TYPE *iet, M_DATA_TYPE *jet, M_DATA_TYPE *gm, int32_t dim, int32_t i, int32_t j, M_DATA_TYPE *newm) {
    JCB_doGAG(it, jt, dim, gm[0], gm[1], i, j, newm);
    JCB_doAG(iet, jet, dim, gm[0], gm[1], i, j);
}

int32_t JCB_convergence(M_DATA_TYPE *m1, M_DATA_TYPE *old, int32_t dim, int32_t row1, int32_t row2) {
    int32_t ret = 1;
    int32_t i = 0;

    if (m1[row1] != old[row1]) {
        old[row1] = m1[row1];
        ret = 0;
    }

    if (m1[row2] != old[row2]) {
        old[row2] = m1[row2];
        ret = 0;
    }

    return ret;
}

void update_pivot_in_row(M_DATA_TYPE *mtemp, int32_t row, int32_t *ppos, int32_t dim) {
    int32_t pos = row + 1;
    int32_t i;

    for (i=pos+1; i<dim; i++) {
        if (JCB_abs(mtemp[pos]) < JCB_abs(mtemp[i])) {
            pos = i;
        }
    }

    *ppos = pos;
}

void JCB_checkRemainPivotValue(M_DATA_TYPE *row1temp, M_DATA_TYPE *row2temp, M_DATA_TYPE *pvtval, int32_t *pvtpos, int32_t row1, int32_t row2, int32_t dim) {
    int32_t i;
    for (i=0; i<row2+1; i++) {
        int32_t trow2 = pvtpos[i];
        if (i==row1 || i==row2)
            continue;
        if (trow2 == row1) {
            pvtval[i] = row1temp[i];
        } else if (trow2 == row2){
            pvtval[i] = row2temp[i];
        }
    }
}

void JCB_update_pivot_single_1(M_DATA_TYPE *m1, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row, int32_t dim) {
    uint32_t i;
    int32_t pos;

    update_pivot_in_row(m1, row, &pos, dim);
    pvtpos[row] = pos;
    pvtval[row] = m1[pos];
}

void JCB_update_pivot_single_flow(M_DATA_TYPE *m1, M_DATA_TYPE *m2, M_DATA_TYPE *m1f, M_DATA_TYPE *m2f, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row1, int32_t row2, int32_t *ppos, int32_t dim) {
    update_pivot_in_row(m1, row1, ppos, dim);
    JCB_checkRemainPivotValue(m1f, m2f, pvtval, pvtpos, row1, row2, dim);
}

void JCB_update_pivot_single_2(M_DATA_TYPE *m1, M_DATA_TYPE *m2, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row1, int32_t row2, int32_t dim) {
    uint32_t i;
    int32_t pos;
    M_DATA_TYPE m1f[MAX_SIZE];
    M_DATA_TYPE m2f[MAX_SIZE];

    for (i=0; i<dim; i++) {
        if (i<row1)
            m1f[i] = m1[i];
        if (i<row2)
            m2f[i] = m2[i];
    }

    JCB_update_pivot_single_flow(m1, m2, m1f, m2f, pvtpos, pvtval, row1, row2, &pos, dim);
    pvtpos[row1] = pos;
    pvtval[row1] = m1[pos];
}

void JCB_update_pivot_double_flow(M_DATA_TYPE *mtemp1, M_DATA_TYPE *mtemp2, M_DATA_TYPE *m1f, M_DATA_TYPE *m2f, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row1, int32_t row2, int32_t *ppos1, int32_t *ppos2, int32_t dim) {
    update_pivot_in_row(mtemp1, row1, ppos1, dim);
    update_pivot_in_row(mtemp2, row2, ppos2, dim);
    JCB_checkRemainPivotValue(m1f, m2f, pvtval, pvtpos, row1, row2, dim);
}

void JCB_update_pivot_double(M_DATA_TYPE *m1, M_DATA_TYPE *m2, int32_t *pvtpos, M_DATA_TYPE *pvtval, int32_t row1, int32_t row2, int32_t dim) {
    int32_t pos1, pos2;
    uint32_t i;
    M_DATA_TYPE m1f[MAX_SIZE];
    M_DATA_TYPE m2f[MAX_SIZE];

    for (i=0; i<dim; i++) {
        if (i<row1)
            m1f[i] = m1[i];
        if (i<row2)
            m2f[i] = m2[i];
    }

    JCB_update_pivot_double_flow(m1, m2, m1f, m2f, pvtpos, pvtval, row1, row2, &pos1, &pos2, dim);
    pvtpos[row1] = pos1;
    pvtval[row1] = m1[pos1];
    pvtpos[row2] = pos2;
    pvtval[row2] = m2[pos2];
}

int32_t JCB_findLargest(M_DATA_TYPE *pvtval, int32_t dim) {
    int32_t i, pos = 0;

    for (i=1; i<dim-1; i++) {
        if (JCB_abs(pvtval[pos]) < JCB_abs(pvtval[i]))
            pos = i;
    }

    return pos;
}

void JCB_checkCache(M_DATA_TYPE *m,
                    M_DATA_TYPE *evs,
                    int32_t or1,
                    int32_t or2,
                    int32_t r1,
                    int32_t r2,
                    M_DATA_TYPE *rt1,
                    M_DATA_TYPE *rt2,
                    M_DATA_TYPE *et1,
                    M_DATA_TYPE *et2,
                    int32_t dim) {
    int32_t i;
    if (or1 != -1  && (or1 != r1 || r1 == -1)) {
        for (i=0; i<dim; i++) {
            m[or1*dim+i] = rt1[i];
            m[i*dim+or1] = rt1[i];
            evs[i*dim+or1] = et1[i];
        }
    }

    if (or2 != -1  && (or2 != r2 || r2 == -1)) {
        for (i=0; i<dim; i++) {
            m[or2*dim+i] = rt2[i];
            m[i*dim+or2] = rt2[i];
            evs[i*dim+or2] = et2[i];
        }
    }

    if (r1 != -1  && or1 != r1) {
        for (i=0; i<dim; i++) {
            rt1[i] = m[r1*dim+i];
            et1[i] = evs[i*dim+r1];
        }
    }
    if (r2 != -1  && or2 != r2) {
        for (i=0; i<dim; i++) {
            rt2[i] = m[r2*dim+i];
            et2[i] = evs[i*dim+r2];
        }
    }
    if (r2 != -1 && or2 == r2 && or1 != r1) {
        rt1[r2] = rt2[r1];
    }
    if (r2 != -1 && or2 != r2 && or1 == r1) {
        rt2[r1] = rt1[r2];
    }
}

void JCB_doCheckCacheandGivens(M_DATA_TYPE *m,
                               M_DATA_TYPE *evectors,
                               M_DATA_TYPE *gm,
                               int32_t oldrow1,
                               int32_t oldrow2,
                               int32_t row1,
                               int32_t row2,
                               M_DATA_TYPE *row1temp,
                               M_DATA_TYPE *row2temp,
                               M_DATA_TYPE *evs1temp,
                               M_DATA_TYPE *evs2temp,
                               M_DATA_TYPE e1,
                               M_DATA_TYPE e2,
                               M_DATA_TYPE e3,
                               int32_t dim) {
    JCB_checkCache(m, evectors, oldrow1, oldrow2, row1, row2, row1temp, row2temp, evs1temp, evs2temp, dim);
    JCB_Calc_Givens(e1, e2, e3, gm, dim, row1, row2);
}

void JCB_doFinal(M_DATA_TYPE *m,
                 M_DATA_TYPE *evectors,
                 M_DATA_TYPE *out_matrix1,
                 M_DATA_TYPE *old,
                 M_DATA_TYPE *row1temp,
                 M_DATA_TYPE *row2temp,
                 M_DATA_TYPE *evs1temp,
                 M_DATA_TYPE *evs2temp,
                 int32_t oldrow1,
                 int32_t oldrow2,
                 int32_t dim) {
    int32_t i;
    JCB_checkCache(m, evectors, oldrow1, oldrow2, -1, -1, row1temp, row2temp, evs1temp, evs2temp, dim);

    for (i=0; i<dim; i++) {
        out_matrix1[i] = old[i];
    }
}

void JCB_Symm(M_DATA_TYPE *in_matrix,
              M_DATA_TYPE *out_matrix1,
              int32_t *counts,
              M_DATA_TYPE *out_matrix2,
              int32_t dim) {

    int32_t i = 0, iters = 0;
    int32_t convergence = 0;
    int32_t oldrow1 = -1, oldrow2 = -1;
    int32_t pvtpos[MAX_SIZE];
    M_DATA_TYPE pvtval[MAX_SIZE];
    M_DATA_TYPE old[MAX_SIZE];
    M_DATA_TYPE newm[MAX_SIZE];
    M_DATA_TYPE row1temp[MAX_SIZE];
    M_DATA_TYPE row2temp[MAX_SIZE];
    M_DATA_TYPE evs1temp[MAX_SIZE];
    M_DATA_TYPE evs2temp[MAX_SIZE];

    M_DATA_TYPE *m = in_matrix;
    M_DATA_TYPE *evectors = out_matrix2;

    for (i=0; i<dim; i++) {
        newm[i] = m[i*dim+i];
        old[i] = m[i*dim+i];
        evectors[i*dim+i] = 1.0;
    }

    for (i=0; i<dim-1; i++) {
        int32_t j;

        for (j=i+1; j<dim; j++) {
            row1temp[j] = m[i*dim+j];
        }
        JCB_update_pivot_single_1(row1temp, pvtpos, pvtval, i, dim);
    }

    while (!convergence) {
        M_DATA_TYPE e1, e2, e3;
        int32_t row1, row2;

        row1 = JCB_findLargest(pvtval, dim);
        row2 = pvtpos[row1];
        e1 = old[row1];
        e3 = old[row2];
        e2 = pvtval[row1];

        if (e2 != 0) {
            static M_DATA_TYPE gm[2];
            JCB_doCheckCacheandGivens(m, evectors, gm,  oldrow1, oldrow2, row1, row2, row1temp, row2temp, evs1temp, evs2temp, e1, e2, e3, dim);
            JCB_doCalc(row1temp, row2temp, evs1temp, evs2temp, gm, dim, row1, row2, newm);

            if (row2 != dim-1) {
                JCB_update_pivot_double(row1temp, row2temp, pvtpos, pvtval, row1, row2, dim);
            } else {
                JCB_update_pivot_single_2(row1temp, row2temp, pvtpos, pvtval, row1, row2, dim);
            }
            oldrow1 = row1;
            oldrow2 = row2;
        }
        convergence = JCB_convergence(newm, old, dim, row1, row2);
        iters ++;
    }
    JCB_doFinal(m, evectors, out_matrix1, old, row1temp, row2temp, evs1temp, evs2temp, oldrow1, oldrow2, dim);
    counts[0] = iters;
}

uint64_t t_diff(struct timespec s, struct timespec e) {
    struct timespec t;

    if ((e.tv_nsec - s.tv_nsec) < 0) {
        t.tv_sec = e.tv_sec - s.tv_sec - 1;
        t.tv_nsec = 1000000000 + e.tv_nsec - s.tv_nsec;
    } else {
        t.tv_sec = e.tv_sec - s.tv_sec;
        t.tv_nsec = e.tv_nsec - s.tv_nsec;
    }
    return (uint64_t)t.tv_sec*1000000000 + t.tv_nsec;
}

int32_t main(int32_t argc, char *argv[]) {
    //M_DATA_TYPE data[16] = {4,-30,60,-35,-30,300,-675,420,60,-675,1620,-1050,-35,420,-1050,700};
    //M_DATA_TYPE in[SIZE] = {0, -6, 4, -6, 0, 7, 5, 7, 0};
    M_DATA_TYPE *in, *out1, *out2;
    int32_t dim, ifprint, i;
    int32_t counts[1] = {0};
    struct timespec s, e;
    uint64_t times;

    if (argc < 3) {
        printf("Usage: %s <dimension size> <if print>\n", argv[0]);
        return -1;
    }

    dim = atoi(argv[1]);
    if (dim < 2) {
        printf("Dimension cannot smaller than 2\n");
        return -1;
    }

    ifprint = atoi(argv[2]);

    in = malloc(dim*dim*sizeof(M_DATA_TYPE));
    out1 = malloc(dim*sizeof(M_DATA_TYPE));
    out2 = malloc(dim*dim*sizeof(M_DATA_TYPE));

    for (i=0; i<dim; i++) {
        int32_t j;
        for (j=0; j<dim; j++) {
            in[i*dim+j] = j+1;
            in[j*dim+i] = j+1;
            //in[i*dim+j] = data[i*dim+j];
            out2[i*dim+j] = 0.0;
        }
        out1[i] = 0.0;
    }

    clock_gettime(CLOCK_MONOTONIC, &s);
    JCB_Symm(in, out1, counts, out2, dim);
    clock_gettime(CLOCK_MONOTONIC, &e);

    times = t_diff(s,e);

    
    printf("Time : %" PRIu64 " \n", times);
    printf("Iteration: %d\n", counts[0]);

    if (ifprint) {
        printMatrix(in,dim);

        for (i=0; i<dim; i++) {
            printf("%f, ", out1[i]);
        }
        printf("\n\n");
        printMatrix(out2, dim);
    }

    free(in);
    free(out1);
    free(out2);

    return 0;
}
