#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <inttypes.h>

#ifdef HPC
#define MAX_SIZE 10000
#else
#define MAX_SIZE 100
#endif
#define M_DATA_TYPE float

static uint32_t msize;

void printMatrix(M_DATA_TYPE *m, int32_t dim) {
    uint32_t i;
    for (i=0; i<dim; i++) {
        uint32_t j = 0;
        for (; j<dim; j++) {
            printf("%f, ", m[i*dim+j]);
        }
        printf("\n");
    }
    printf("\n\n");
}


M_DATA_TYPE QR_copy_sign(M_DATA_TYPE num, M_DATA_TYPE val) {
    if (val < 0)
        return -num;
    return num;
}

M_DATA_TYPE QR_abs(M_DATA_TYPE val) {
    if (val < 0.0)
        return -val;
    return val;
}

void QR_Calc_Givens(M_DATA_TYPE *gm,
                    M_DATA_TYPE e1,
                    M_DATA_TYPE e2,
                    int32_t dim) {
    int32_t i, j;
    M_DATA_TYPE sin, cos, tan, radius;

    if (e2 == 0.0) {
        cos = QR_copy_sign(1, e1);
        sin = 0;
        //radius = QR_abs(e1);
    } else if (e1 == 0.0) {
        cos = 0;
        sin = -QR_copy_sign(1, e2);
        //radius = QR_abs(e2);
    } else if (QR_abs(e2) > QR_abs(e1)) {
        M_DATA_TYPE u = 0.0, t;
        tan = e1/e2;
        t = sqrt(1+tan*tan);
        u = QR_copy_sign(t, e2);
        sin = -1/u;
        cos = -sin*tan;
        //radius = u*e2;
    } else {
        M_DATA_TYPE u = 0.0, t;
        tan = e2/e1;
        t = sqrt(1+tan*tan);
        u = QR_copy_sign(t, e1);
        cos = 1/u;
        sin = -cos*tan;
        //radius = u*e1;
    }
    gm[0] = sin;
    gm[1] = cos;
}

void QR_doGAG(M_DATA_TYPE *m, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y) {
    M_DATA_TYPE temp1[MAX_SIZE];
    M_DATA_TYPE temp2[MAX_SIZE];
    M_DATA_TYPE temp3[MAX_SIZE];
    M_DATA_TYPE temp4[MAX_SIZE];
    M_DATA_TYPE a[MAX_SIZE];
    M_DATA_TYPE b[MAX_SIZE];
    int32_t i;

    for (i=0; i<dim; i++) {
        a[i] = m[x*dim+i];
        b[i] = m[y*dim+i];
        temp1[i] = a[i] * cos - b[i] * sin;
        temp2[i] = a[i] * sin + b[i] * cos;
        m[x*dim+i] = temp1[i];
        m[y*dim+i] = temp2[i];
    }

    a[x] = temp1[x];
    a[y] = temp2[x];
    b[x] = temp1[y];
    b[y] = temp2[y];

    for (i=0; i<dim; i++) {
        temp1[i] = a[i] * cos - b[i] * sin;
        temp2[i] = a[i] * sin + b[i] * cos;
        m[i*dim+x] = temp1[i];
        m[i*dim+y] = temp2[i];
    }
}

void QR_doAG(M_DATA_TYPE *m, int32_t dim, M_DATA_TYPE sin, M_DATA_TYPE cos, int32_t x, int32_t y) {
    M_DATA_TYPE a[MAX_SIZE];
    M_DATA_TYPE b[MAX_SIZE];
    int32_t i;

    for (i=0; i<dim; i++) {
        a[i] = m[i*dim+x];
        b[i] = m[i*dim+y];
        m[i*dim+x] = a[i] * cos - b[i] * sin;
        m[i*dim+y] = a[i] * sin + b[i] * cos;
    }
}

void QR_checkdiagonal(M_DATA_TYPE *m, int32_t dim, M_DATA_TYPE *shift_val) {
    int32_t ret = 1, i;

    for (i=0; i<dim; i++) {
        if (*shift_val != m[i*dim+i])
            ret = 0;
        m[i*dim+i] -= *shift_val;
    }

    if (ret) {
        *shift_val = *shift_val - 1;
        for (i=0; i<dim; i++) {
            m[i*dim+i] += 1;
        }
    }
}

int32_t QR_convergence(M_DATA_TYPE *m1, M_DATA_TYPE *old, int32_t dim, M_DATA_TYPE shift_val) {
    int32_t ret = 1;
    int32_t i = 0;

    for (i=0; i<dim; i++) {
        m1[i*dim+i] += shift_val;
        if (old[i] != m1[i*dim+i]) {
            old[i] = m1[i*dim+i];
            ret = 0;
        }
    }

    return ret;
}

void QR_doCalc(M_DATA_TYPE *space1, M_DATA_TYPE *space2, M_DATA_TYPE *gm, int32_t dim, int32_t i, int32_t j) {
    QR_doGAG(space1, dim, gm[0], gm[1], i, j);
    QR_doAG(space2, dim, gm[0], gm[1], i, j);
}

void QR_Symm(M_DATA_TYPE *in_matrix,
             M_DATA_TYPE *out_matrix1,
             int32_t *counts,
             M_DATA_TYPE *out_matrix2,
             int32_t dim) {

    int32_t i = 0, iters = 0;
    uint32_t start = 0;
    int32_t convergence = 0;
    M_DATA_TYPE *space1 = in_matrix;
    M_DATA_TYPE *space2 = out_matrix2;
    M_DATA_TYPE space4[MAX_SIZE];

    for (i=0; i<dim*dim; i++) {
        if (i % (dim+1) == 0)
            space2[i] = 1.0;
        else
            space2[i] = 0.0;
    }

    for (i=1; i<dim-1; i++) {
        uint32_t j;
        for (j=i+1; j<dim; j++) {
            M_DATA_TYPE e1 = space1[i*dim+(i-1)];
            M_DATA_TYPE e2 = space1[j*dim+(i-1)];
            if (QR_abs(e2) > 0.0000001) {
                uint32_t idiff = (i-1);
                static M_DATA_TYPE tgm[2];
                QR_Calc_Givens(tgm, e1, e2, dim);
                QR_doCalc(space1, space2, tgm, dim, i, j);
            }
        }
    }

    while (!convergence) {
        M_DATA_TYPE *m = space1;
        M_DATA_TYPE shift_val = m[(dim-1)*dim + (dim-1)];
        QR_checkdiagonal(m, dim, &shift_val);

        for (i=0; i<dim-1; i++) {
            int32_t j = i+1;
            static M_DATA_TYPE gm[2];
            M_DATA_TYPE e1 = m[i*dim+i];
            M_DATA_TYPE e2 = m[j*dim+i];
            if (QR_abs(e2) > 0.0000001) {
                QR_Calc_Givens(gm, e1, e2, dim);
                QR_doCalc(space1, space2, gm, dim, i, j);
            }
        }

        convergence = QR_convergence(m, space4, dim, shift_val);
        iters++;
    }
    for (i=0; i<dim; i++) {
        out_matrix1[i] = space4[i];
    }
    counts[0] = iters;
}

uint64_t t_diff(struct timespec s, struct timespec e) {
    struct timespec t;

    if ((e.tv_nsec - s.tv_nsec) < 0) {
        t.tv_sec = e.tv_sec - s.tv_sec - 1;
        t.tv_nsec = 1000000000 + e.tv_nsec - s.tv_nsec;
    } else {
        t.tv_sec = e.tv_sec - s.tv_sec;
        t.tv_nsec = e.tv_nsec - s.tv_nsec;
    }
    return (uint64_t)t.tv_sec*1000000000 + t.tv_nsec;
}

int main(int32_t argc, char *argv[]) {
    //M_DATA_TYPE data[16] = {4,-30,60,-35,-30,300,-675,420,60,-675,1620,-1050,-35,420,-1050,700};
    //M_DATA_TYPE in[SIZE] = {0, -6, 4, -6, 0, 7, 5, 7, 0};
    M_DATA_TYPE *in, *out1, *out2;
    int32_t dim, ifprint, i;
    int32_t counts[1] = {0};
    struct timespec s, e;
    uint64_t times;

    if (argc < 3) {
        printf("Usage: %s <dimension size> <if print>\n", argv[0]);
        return -1;
    }

    dim = atoi(argv[1]);
    if (dim < 2) {
        printf("Dimension cannot smaller than 2\n");
        return -1;
    }

    ifprint = atoi(argv[2]);

    in = malloc(dim*dim*sizeof(M_DATA_TYPE));
    out1 = malloc(dim*sizeof(M_DATA_TYPE));
    out2 = malloc(dim*dim*sizeof(M_DATA_TYPE));

    for (i=0; i<dim; i++) {
        int32_t j;
        for (j=0; j<dim; j++) {
            in[i*dim+j] = j+1;
            in[j*dim+i] = j+1;
            //in[i*dim+j] = data[i*dim+j];
            out2[i*dim+j] = 0.0;
        }
        out1[i] = 0.0;
    }

    clock_gettime(CLOCK_MONOTONIC, &s);
    QR_Symm(in, out1, counts, out2, dim);
    clock_gettime(CLOCK_MONOTONIC, &e);

    times = t_diff(s, e);

    printf("Time : %" PRIu64 " \n",times);
    printf("Iteration: %d\n", counts[0]);

    if (ifprint) {
        printMatrix(in,dim);

        for (i=0; i<dim; i++) {
            printf("%f, ", out1[i]);
        }
        printf("\n\n");
        printMatrix(out2, dim);
    }

    free(in);
    free(out1);
    free(out2);

    return 0;
}
