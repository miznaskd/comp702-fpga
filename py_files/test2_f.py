from pynq import DefaultIP
from pynq import Overlay
from pynq import Xlnk
import numpy as np
import time
import sys

class JCBSymmDriver(DefaultIP):
    def __init__(self, description):
        super().__init__(description=description)

    bindto = ['xilinx.com:hls:JCB_Symm:1.0']

    @property
    def status(self):
        return self.read(0x00)

    @status.setter
    def status(self, value):
        self.write(0x00, value)

    @property
    def input(self):
        return self.read(0x10)

    @input.setter
    def input(self, addr):
        self.write(0x10, addr)

    @property
    def output1(self):
        return self.read(0x18)

    @output1.setter
    def output1(self, addr):
        self.write(0x18, addr)

    @property
    def output2(self):
        return self.read(0x20)

    @output2.setter
    def output2(self, addr):
        self.write(0x20, addr)

    @property
    def dimension(self):
        return self.read(0x28)

    @dimension.setter
    def dimension(self, value):
        self.write(0x28, value)


def printMatrix(m,dim):
    for index, item in enumerate(m, start=1):
        print('%f, ' % item, end='')
        if not index % dim:
            print()
    print()

def printMatrix1(m,dim):
    for index, item in enumerate(m, start=1):
        print('%f, ' % item, end='')
    print()
    print()

if __name__ == '__main__':

    #l = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,11,13,17,19,23,29,31,37,47,59]
    #l = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,18]
    #l = [4,-30,60,-35, -30, 300, -675, 420, 60, -675, 1620, -1050, -35, 420, -1050, 700]

    if len(sys.argv) < 3:
        print("Usage: %s <dimension size> <printmatrix>" % sys.argv[0])
        exit(1)
    
    dim = int(sys.argv[1])
    ifprint = int(sys.argv[2])
    if dim < 2:
        print("Dimension should larger than 1")
        exit(1)

    l = [0] * (dim*dim)

    for i in range(0, dim):
        for j in range(0,dim):
             l[i*dim+j] = j+1
             l[j*dim+i] = j+1

    overlay = Overlay('/home/xilinx/pynq/overlays/All_Symm/All_Symm.bit')

    t0 = time.time()
    # defined in vivado project
    JCB_Symm = overlay.JCB_Symm_0

    xlnk = Xlnk()
    in_buffer = xlnk.cma_array(shape=(dim*dim,), dtype=np.float32)
    out_buffer1 = xlnk.cma_array(shape=(dim,), dtype=np.float32)
    out_buffer2 = xlnk.cma_array(shape=(dim*dim,), dtype=np.float32)

    #print(in_buffer.physical_address)
    #print(out_buffer1.physical_address)
    #print(out_buffer2.physical_address)

    '''
    Status 0x01 for ap_start (read/write/COH)
    Status 0x02 for ap_done (read/COR)
    Status 0x04 for ap_idle (read)
    Status 0x08 for ap_ready (read)
    Status 0x80 for ap_reset (read/write)
    '''

    ap_start = 0x01
    ap_done = 0x02
    ap_idle = 0x04
    ap_ready = 0x08
    ap_reset = 0x80

    pos = 0
    for i in l:
        in_buffer[pos] = i
        pos += 1

    #printMatrix(in_buffer, dim)
    #printMatrix(out_buffer2, dim)

    t2 = time.time()
    if (JCB_Symm.status & ap_idle) == ap_idle:
        status = 0
        JCB_Symm.dimension = dim
        JCB_Symm.input = in_buffer.physical_address
        JCB_Symm.output1 = out_buffer1.physical_address
        JCB_Symm.output2 = out_buffer2.physical_address
        JCB_Symm.status = ap_start
        while (status & ap_done) != ap_done:
            status = JCB_Symm.status
    t3 = time.time()
    t1 = time.time()
    print(t3-t2)
    print(t1-t0)
    print(in_buffer[1])

    if ifprint:
        print(status)
        printMatrix(in_buffer, dim)
        printMatrix1(out_buffer1, dim)
        printMatrix(out_buffer2, dim)
