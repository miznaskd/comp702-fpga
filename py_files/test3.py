import numpy as np
import sys

if __name__ == '__main__':

    dim = int(sys.argv[1])

    l = [0] * (dim*dim)

    for i in range(0, dim):
        for j in range(0,dim):
             l[i*dim+j] = j+1
             l[j*dim+i] = j+1

    ll = []
    for i in range(0,dim):
        ll.append([0] * dim)

    for i in range(0, dim):
        for j in range(0,dim):
            ll[i][j] = l[i*dim+j]

    val, vec = np.linalg.eig(ll)

    print(val)
