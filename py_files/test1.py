import numpy as np
import time
from math import sqrt

def printMatrix(m,dim):
    for index, item in enumerate(m, start=1):
        print('%f, ' % item, end='')
        if not index % dim:
            print()
    print()

def CopySign(num, value):
    if value < 0:
        return -num
    else:
        return num

def Calc_Givens(m, dim, p, q, diff):
    e1 = m[p*dim+(p-diff)]
    e2 = m[q*dim+(p-diff)]

    c = 0
    s = 0
    r = 0
    t = 0
    if e2 == 0:
        c = CopySign(1, e1)
        s = 0
        r = abs(e1)
    elif e1 == 0:
        c = 0
        s = -CopySign(1,e2)
        r = abs(e2)
    elif abs(e2) > abs(e1):
        t = e1/e2
        u = CopySign(sqrt(1+t*t), e2)
        s = -1/u
        c = -s*t
        r = e2*u
    else:
        t = e2/e1
        u = CopySign(sqrt(1+t*t), e1)
        c = 1/u
        s = -c*t
        r = e1*u

    return s, c

def QR_doGA(m, dim, s, c, x, y):
    for i in range(0,dim):
        a = m[x*dim+i]
        b = m[y*dim+i]
        m[x*dim+i] = a*c - b*s
        m[y*dim+i] = a*s + b*c

def QR_doAG(m, dim, s, c, x, y):
    for i in range(0,dim):
        a = m[i*dim+x]
        b = m[i*dim+y]
        m[i*dim+x] = a*c - b*s
        m[i*dim+y] = a*s + b*c

def QR_doGAG(m, dim, s, c, x, y):
    QR_doGA(m, dim, s, c, x, y)
    QR_doAG(m, dim, s, c, x, y)

def QR_convergence(m1, old, dim):
    ret = 0
    for i in range(0, dim):
        pos = i*dim + i
        if (m1[pos] == old[i]):
            ret = 1
        else:
            ret = 0
            break
    return ret

def QR_checkdiagonal(m, dim):
    ret = 0
    e = m[(dim-1)*dim+(dim-1)]
    for i in range(0,dim-1):
        if m[i] == e:
            ret = 1
        else:
            ret = 0
            break
    return ret

def QR_Symm(a, dim, evals, eiv2):
    start = 0
    setgm = 0
    convergence = 0
    old = np.array([0]*dim, dtype=np.float)

    for k in range(0, dim):
        eiv2[k*dim+k] = 1.0

    for i in range(1,dim-1):
        for j in range(i+1, dim):
            if abs(a[j*dim+(i-1)]) > 0.0000001:
                s,c = Calc_Givens(a,dim,i,j, 1)
                QR_doGAG(a, dim, s, c, i, j)
                QR_doAG(eiv2, dim, s, c, i, j)

    while not convergence:
        shift_val = a[(dim-1)*dim + (dim-1)]

        if QR_checkdiagonal(a, dim):
            shift_val -= 1
        
        for i in range(0, dim):
            old[i] = a[i*dim+i]
            a[i*dim+i] -= shift_val

        for i in range(0, dim-1):
            j = i + 1
            s,c = Calc_Givens(a,dim,i,j, 0)
            QR_doGAG(a, dim, s, c, i, j)
            QR_doAG(eiv2, dim, s, c, i, j)

        for i in range(0, dim):
            a[i*dim+i] += shift_val
        
        convergence = QR_convergence(a, old, dim)
    
    for i in range(0, dim):
        evals[i] = old[i]

if __name__ == '__main__':
    dim = 4
    a = np.array([4,-30,60,-35, -30, 300, -675, 420, 60, -675, 1620, -1050, -35, 420, -1050, 700], dtype=np.float)
    eiv2 = np.array([0]*(dim*dim), dtype=np.float)
    evals = np.array([0]*dim, dtype=np.float)

    t0 = time.time()
    QR_Symm(a, dim, evals, eiv2)
    t1 = time.time()
    print(t1-t0)

    for i in range(0, dim):
        print(evals[i], end=' ')
    print('\n')
    printMatrix(eiv2, dim)
