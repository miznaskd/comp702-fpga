import numpy as np
import time
from math import sqrt

def printMatrix(m,dim):
    for index, item in enumerate(m, start=1):
        print('%f, ' % item, end='')
        if not index % dim:
            print()
    print()

def Calc_Givens(m, dim, p, q):
    e1 = m[p*dim+p]
    e2 = m[q*dim+p]
    e3 = m[q*dim+q]

    w = (e3 - e1) / (2*e2)
    t = 0.0
    if w < 0:
        t = -w - sqrt((w*w)+1)
    else:
        t = -w + sqrt((w*w)+1)
    s = t / sqrt(1+(t*t))
    c = 1 / sqrt(1+(t*t))

    return s,c

def JCB_doGA(m, dim, s, c, x, y):
    for i in range(0,dim):
        a = m[x*dim+i]
        b = m[y*dim+i]
        m[x*dim+i] = a*c - b*s
        m[y*dim+i] = a*s + b*c

def JCB_doAG(m, dim, s, c, x, y):
    for i in range(0,dim):
        a = m[i*dim+x]
        b = m[i*dim+y]
        m[i*dim+x] = a*c - b*s
        m[i*dim+y] = a*s + b*c

def JCB_doGAG(m, dim, sin, cos, x, y):
    a = np.array([0]*dim, dtype=np.float)
    b = np.array([0]*dim, dtype=np.float)
    c = np.array([0]*dim, dtype=np.float)
    d = np.array([0]*dim, dtype=np.float)

    for i in range(0, dim):
        a[i] = m[x*dim+i]
        b[i] = m[y*dim+i]
        c[i] = m[i*dim+x]
        d[i] = m[i*dim+y]

    for i in range(0, dim):
        m[x*dim+i] = a[i]*cos - b[i]*sin
        m[y*dim+i] = a[i]*sin + b[i]*cos
        m[i*dim+x] = c[i]*cos - d[i]*sin
        m[i*dim+y] = c[i]*sin + d[i]*cos

    m[x*dim+y] = 0.0
    m[y*dim+x] = 0.0
    m[x*dim+x] = cos * cos * a[x] - 2 * cos * sin * a[y] + sin * sin * b[y]
    m[y*dim+y] = sin * sin * a[x] + 2 * cos * sin * a[y] + cos * cos * b[y]


def JCB_convergence(m1, old, dim):
    ret = 0
    for i in range(0, dim):
        pos = i*dim + i
        if (m1[pos] == old[i]):
            ret = 1
        else:
            ret = 0
            break
    return ret

def update_pivot_onerow(a, ppos, row_num, dim):
    row_pos = row_num * dim + row_num
    row_last = row_num * dim + dim
    pos = row_pos + 1
    for j in range(row_pos+2, row_last):
        if abs(a[pos]) < abs(a[j]):
            pos = j
    ppos[row_num] = pos

def JCB_Symm(a, dim, evals, evs):
    convergence = 0
    start = 0
    count = 0
    old = np.array([0]*dim, dtype=np.float)
    pvspos = np.array([0]*dim, dtype=np.int32)

    for i in range(0, dim-1):
        update_pivot_onerow(a, pvspos, i, dim)

    for j in range(0, dim):
        evs[j*dim+j] = 1.0

    while not convergence:
        count = count + 1
        for i in range(0,dim):
            old[i] = a[i*dim+i]
        k = 0
        for i in range(1, dim-1):
            x = pvspos[i]
            y = pvspos[k]
            if abs(a[x]) > abs(a[y]):
                k = i

        l = pvspos[k] - k*dim
        if abs(a[k*dim+l]) > 0:
            s,c = Calc_Givens(a, dim, k, l)
            JCB_doGAG(a, dim, s, c, k, l)
            JCB_doAG(evs, dim, s, c, k, l)

            update_pivot_onerow(a, pvspos, k, dim)
            if l != (dim-1):
                update_pivot_onerow(a, pvspos, l, dim)

        convergence = JCB_convergence(a, old, dim)
    
    for i in range(0, dim):
        evals[i] = old[i]
    print(count)

if __name__ == '__main__':
    dim = 4
    a = np.array([4,-30,60,-35, -30, 300, -675, 420, 60, -675, 1620, -1050, -35, 420, -1050, 700], dtype=np.float)
    evs = np.array([0]*(dim*dim), dtype=np.float)
    evals = np.array([0]*dim, dtype=np.float)

    t0 = time.time()
    JCB_Symm(a, dim, evals, evs)
    t1 = time.time()
    print(t1-t0)
    for i in range(0, dim):
        print(evals[i], end=' ')
    print('\n')
    printMatrix(evs, dim)
